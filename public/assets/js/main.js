$('document').ready( function(){

	$('.fancy').fancybox({
		padding: 10
	});

	$('.cancelamento a.fancy').fancybox({
		padding: 10,
		type : 'ajax',
		title : ''
	});

	$(window).scroll(function(){
		var scrollPos = $(window).scrollTop();
		$('#intro').css('background-position','center bottom ' + (scrollPos/5) + 'px');
		$('.titulo').css('opacity', '1' - (scrollPos/400));
	});

	if(jQuery().cycle){
		if($('#apresentacao #banners .banner').length > 1){
			$('#apresentacao #banners').cycle({
				prev : $('#banners-nav-prev'),
				next : $('#banners-nav-next'),
				fx : 'scrollHorz'
			});
		}else{
			$('#banners-nav-prev, #banners-nav-next').hide();
		}
	}

	$('.main-lazer #lista-item li a').click( function(e){
		e.preventDefault();
		var glr = $(this).attr('data-galeria');
		if(!$(this).hasClass('ativo')){
			$('.main-lazer #lista-item li a.ativo').removeClass('ativo');
			$(this).addClass('ativo');
			if($('#galeria-viewport .galeria.aberta').length){
				// fechar galeria aberta
				$('#galeria-viewport .galeria.aberta').removeClass('imgsAbertas');
				setTimeout( function(){
					$('#galeria-viewport .galeria.aberta').removeClass('aberta');
					// abrir galeria nova
					$('#galeria-viewport .galeria-'+glr).addClass('carregando').addClass('aberta');
					$('#galeria-viewport .galeria-'+glr).addClass('imgsAbertas');
					setTimeout( function(){
						$('#galeria-viewport .galeria-'+glr).removeClass('carregando');
					}, 1000);
				}, 1100);		
			}else{
				// abrir galeria nova
				$('#galeria-viewport .galeria-'+glr).addClass('carregando').addClass('aberta');
				$('#galeria-viewport .galeria-'+glr).addClass('imgsAbertas');
				setTimeout( function(){
					$('#galeria-viewport .galeria-'+glr).removeClass('carregando');
				}, 1000);
			}			
		}
	});

});