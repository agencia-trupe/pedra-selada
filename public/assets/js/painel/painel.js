$('document').ready( function(){

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	bootbox.confirm("Deseja Excluir o Registro?", function(result){
	      	if(result)
	        	form.submit();
	      	else
	        	$(this).modal('hide');
    	});
  	});
    
    $("table.table-sortable tbody").sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    if($('textarea').length){
        $('textarea').ckeditor();

        CKEDITOR.stylesSet.add( 'custom_styles', [
            // Block-level styles
            { name: 'Título', element: 'h2', styles: { 'margin' : '0', 'color' : '#000', 'font-weight' : 'normal', 'font-family' : "'Varela Round', sans-serif", 'font-size' : '15px', 'line-height' : '130%' } },
            { name: 'Parágrafo', element: 'p', styles: { 'margin' : '0', 'color' : '#000', 'font-weight' : 'normal', 'font-family' : "'Varela Round', sans-serif", 'font-size' : '15px', 'line-height' : '130%' } },            
        ]);
    }
});	