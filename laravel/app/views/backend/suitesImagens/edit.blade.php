@section('conteudo')

    <div class="container add">

      	<h2>
        	Alterar Imagem da Suíte: {{$suite->titulo}}
        </h2>  
		
		{{ Form::open( array('route' => array('painel.suitesImagens.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

		    	<div class="form-group">
					<label for="inputLegenda">Legenda</label>
					<input type="text" class="form-control" id="inputLegenda" name="legenda" value="{{ $registro->legenda }}" required>
				</div>

				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					@if($registro->imagem)
						<br><img src="assets/images/suites/imagens/thumbs/{{$registro->imagem}}"><br><br>
					@endif
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div><br>

				<input type="hidden" name="id_suite" value="{{$suite->id}}">

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.suitesImagens.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop