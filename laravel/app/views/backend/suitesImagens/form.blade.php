@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Imagem da Suíte: {{$suite->titulo}}
        </h2>  

		<form action="{{URL::route('painel.suitesImagens.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

		    	<div class="form-group">
					<label for="inputLegenda">Legenda</label>
					<input type="text" class="form-control" id="inputLegenda" name="legenda" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); $s['legenda'] }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" required>
				</div>

				<input type="hidden" name="id_suite" value="{{$suite->id}}">

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.suitesImagens.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop