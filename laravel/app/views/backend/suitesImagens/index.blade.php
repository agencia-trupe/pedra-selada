@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
       <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if($errors->any())
		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	@endif

	<a href="{{URL::route('painel.suites.index')}}" title="Voltar para as Suítes" class="btn btn-default btn-sm">&larr; Voltar para Suítes</a>

	<h2>
  		Imagens da Suíte: {{$suite->titulo}} <a href="{{ URL::route('painel.suitesImagens.create', array('id_suite' => $suite->id)) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span>  Adicionar Imagem</a>
	</h2>


	<table class="table table-striped table-bordered table-hover table-sortable" data-tabela="suites_imagens">
        
        <thead>
    		<tr>
	          	<th>Ordenar</th>
	          	<th>Imagem</th>
	          	<th>Legenda</th>
      			<th><span class="glyphicon glyphicon-cog"></span></th>
    		</tr>
  		</thead>

  		<tbody>
		@if(sizeof($registros))
	    	@foreach ($registros as $registro)

	        	<tr class="tr-row" id="row_{{ $registro->id }}">
	              	<td class="move-actions"><a href="#" class="btn btn-info btn-move btn-sm">mover</a></td>
	              	<td><img src="assets/images/suites/imagens/thumbs/{{$registro->imagem}}"></td>
	              	<td>{{ Str::words($registro->legenda, 15) }}</td>
	              	<td class="crud-actions">
	            		<a href="{{ URL::route('painel.suitesImagens.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

	            	   {{ Form::open(array('route' => array('painel.suitesImagens.destroy', $registro->id), 'method' => 'delete')) }}
	                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
	                   {{ Form::close() }}
	          		</td>
	        	</tr>

	    	@endforeach
		@else
			<tr><td colspan="4"><h3>Nenhum registro encontrado</h3></td></tr>
		@endif
  		</tbody>

    </table>
    
</div>

@stop