@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
       <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if($errors->any())
		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	@endif

	<h2>
  		Lazer <a href="{{ URL::route('painel.lazer.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span>  Adicionar Item de Lazer</a>
	</h2>
	
	<div class="alert alert-block alert-info" style="text-align:center">
		Somente os itens com imagens associadas serão exibidos no site.
	</div>

	<table class="table table-striped table-bordered table-hover table-sortable" data-tabela="lazer">
        
        <thead>
    		<tr>
	          	<th>Ordenar</th>
	          	<th>Título</th>
	          	<th>Imagens</th>
      			<th><span class="glyphicon glyphicon-cog"></span></th>
    		</tr>
  		</thead>

  		<tbody>
		@if(sizeof($registros))
	    	@foreach ($registros as $registro)

	        	<tr class="tr-row" id="row_{{ $registro->id }}">
	              	<td class="move-actions"><a href="#" class="btn btn-info btn-move btn-sm">mover</a></td>
	              	<td>{{$registro->titulo}}</td>
	              	<td><a href="{{URL::route('painel.lazerImagens.index', array('lazer_id' => $registro->id))}}" class="btn btn-sm btn-default" title="Gerenciar imagens do item: {{$registro->titulo}}">gerenciar</a></td>
	              	<td class="crud-actions">
	            		<a href="{{ URL::route('painel.lazer.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

	            	   {{ Form::open(array('route' => array('painel.lazer.destroy', $registro->id), 'method' => 'delete')) }}
	                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
	                   {{ Form::close() }}
	          		</td>
	        	</tr>

	    	@endforeach
		@else
			<tr><td colspan="4"><h3>Nenhum registro encontrado</h3></td></tr>
		@endif
  		</tbody>

    </table>
    
</div>

@stop