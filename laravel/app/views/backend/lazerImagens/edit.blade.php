@section('conteudo')

    <div class="container add">

      	<h2>
        	Alterar Imagens de Lazer do Item: {{$lazer->id}}
        </h2>  
		
		{{ Form::open( array('route' => array('painel.lazerImagens.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

		    	<div class="form-group">
					<label for="inputLegenda">Legenda</label>
					<input type="text" class="form-control" id="inputLegenda" name="legenda" value="{{$registro->legenda}}" required>
				</div>

				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/lazer/thumbs/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

				<input type="hidden" name="lazer_id" value="{{$lazer->id}}">

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.lazerImagens.index', array('lazer_id' => $lazer->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop