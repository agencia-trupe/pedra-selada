@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
       <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if($errors->any())
		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	@endif

	<a href="{{URL::route('painel.lazer.index')}}" title="Voltar para Lazer" class="btn btn-default">&larr; Voltar para Lazer</a>
	
	<h2>
  		Imagens de Lazer do Item: {{$lazer->titulo}} @if(sizeof($registros) < $limiteRegistros) <a href="{{ URL::route('painel.lazerImagens.create', array('lazer_id' => $lazer->id)) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span>  Adicionar Foto de Lazer</a> @endif
	</h2>

	<table class="table table-striped table-bordered table-hover table-sortable" data-tabela="lazer_imagens">
        
        <thead>
    		<tr>
	          	<th>Ordenar</th>
	          	<th>Legenda</th>
	          	<th>Imagem</th>
      			<th><span class="glyphicon glyphicon-cog"></span></th>
    		</tr>
  		</thead>

  		<tbody>
		@if(sizeof($registros))
	    	@foreach ($registros as $registro)

	        	<tr class="tr-row" id="row_{{ $registro->id }}">
	              	<td class="move-actions"><a href="#" class="btn btn-info btn-move btn-sm">mover</a></td>
	              	<td>{{$registro->legenda}}</td>
	              	<td><img src="assets/images/lazer/thumbs/{{$registro->imagem}}"></td>
	              	<td class="crud-actions">
	            		<a href="{{ URL::route('painel.lazerImagens.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

	            	   {{ Form::open(array('route' => array('painel.lazerImagens.destroy', $registro->id), 'method' => 'delete')) }}
	                        <input type="hidden" name="lazer_id" value="{{$lazer->id}}">
	                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
	                   {{ Form::close() }}
	          		</td>
	        	</tr>

	    	@endforeach
		@else
			<tr><td colspan="4"><h3>Nenhum registro encontrado</h3></td></tr>
		@endif
  		</tbody>

    </table>
    
</div>

@stop