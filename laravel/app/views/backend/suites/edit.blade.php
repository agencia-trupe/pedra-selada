@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Suíte
        </h2>  

		{{ Form::open( array('route' => array('painel.suites.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

		    	<div class="form-group">
					<label for="inputTitulo">Título</label>
					<input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{ $registro->titulo }}" required>
				</div>

				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/suites/thumbs/grandes/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem (Thumb)</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

				<div class="form-group">
					@if($registro->imagem_cabecalho)
						Imagem Atual<br>
						<img src="assets/images/suites/cabecalhos/{{$registro->imagem_cabecalho}}"><br>
					@endif
					<label for="inputImagemCabecalho">Imagem (Cabeçalho) - 800px x 200px</label>
					<input type="file" class="form-control" id="inputImagemCabecalho" name="cabecalho">
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto">{{$registro->texto}}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.suites.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop