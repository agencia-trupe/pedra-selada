@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
       <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if($errors->any())
		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	@endif

	<h2>
  		Promoções <a href="{{ URL::route('painel.promocoes.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span>  Adicionar Promoção</a>
	</h2>

	<table class="table table-striped table-bordered table-hover table-sortable" data-tabela="promocoes">
        
        <thead>
    		<tr>
	          	<th>Ordenar</th>
	          	<th>Título</th>
	          	<th>Texto</th>
	          	<th>Imagem</th>
      			<th><span class="glyphicon glyphicon-cog"></span></th>
    		</tr>
  		</thead>

  		<tbody>
		@if(sizeof($registros))
	    	@foreach ($registros as $registro)

	        	<tr class="tr-row" id="row_{{ $registro->id }}">
	              	<td class="move-actions"><a href="#" class="btn btn-info btn-move btn-sm">mover</a></td>
	              	<td>{{$registro->titulo}}</td>
	              	<td>{{ Str::words(strip_tags($registro->texto), 15) }}</td>
	              	<td><img src="assets/images/promocoes/{{$registro->imagem}}" style="max-width:400px;"></td>
	          		<td class="crud-actions">
	            		<a href="{{ URL::route('painel.promocoes.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

	            	   {{ Form::open(array('route' => array('painel.promocoes.destroy', $registro->id), 'method' => 'delete')) }}
	                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
	                   {{ Form::close() }}
	          		</td>
	        	</tr>

	    	@endforeach
		@else
			<tr><td colspan="5"><h3>Nenhum registro encontrado</h3></td></tr>
		@endif
  		</tbody>

    </table>
    
</div>

@stop