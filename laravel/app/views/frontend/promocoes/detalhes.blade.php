@section('conteudo')

	<div class="detalhes">
		<img class="cabecalho" src="assets/images/promocoes/{{$detalhe->imagem}}" alt="{{$detalhe->titulo}}" title="{{$detalhe->titulo}}">

		<h2>{{$detalhe->titulo}}</h2>

		<div class="texto">{{$detalhe->texto}}</div>

		<div class="reserva">
			<a href="reservas" title="Clique aqui e faça sua reserva!">CLIQUE AQUI E FAÇA SUA RESERVA!</a>
		</div>

		<div class="voltar">
			<a href="{{URL::route('promocoes')}}" title="Voltar">&larr; VOLTAR</a>
		</div>
	</div>

@stop