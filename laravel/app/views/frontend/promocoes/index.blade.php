@section('conteudo')

	<div class="lista-promocoes">
		@if(sizeof($promocoes))
			@foreach($promocoes as $promocao)
				<a href="promocoes/detalhes/{{$promocao->slug}}" title="{{$promocao->titulo}}">
					<img src="assets/images/promocoes/{{$promocao->imagem}}" alt="{{$promocao->titulo}}">
					<div class="overlay"></div>
				</a>
			@endforeach
		@else
			<h1 class="naoencontrado">Nenhuma Promoção encontrada.</h1>
		@endif
	</div>

@stop
