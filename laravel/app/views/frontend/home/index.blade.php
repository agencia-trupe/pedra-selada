@section('conteudo')

	<div id="intro">
		<div class="titulo">
			<h1>POUSADA PEDRA SELADA - S 22&deg; 15' 18.4'' W 44&deg; 26' 23.5''</h1>
			<div class="scroll">
				<img src="assets/images/layout/home_scroll.png" alt="Pousada Pedra Selada">
			</div>
		</div>
	</div>

	<div class="conteudo">
		<div id="frase-home">
			<p>A Pousada Pedra Selada é um lugar de sonho para você e sua família desfrutarem de momentos de puro prazer.</p>
			<p>Localizada no coração da serra de Mauá a pousada possui diversas opções de lazer sem abrir mão do conforto e tranquilidade que você merece.</p>
		</div>

		<div id="apresentacao">
			<div class="centro">
				<div class="coluna coluna-texto">
					<p>
						A pousada fica no meio da serra de Mauá, em frente à montanha que lhe dá nome, completamente imersa em verdes, azuis e todos os tons de flores.
					</p>
					<p>
						É o lugar perfeito para proporcionar a você e sua família momentos de aconchego, paz e prazer. Do café fresco no bule sobre o fogão a lenha ao convívio direto com Sérgio, o dono, tudo garante perfeição para estar à vontade e ter vontade de voltar. Sempre...
					</p>
					<h3>se não é o Paraíso, está próximo...</h3>
				</div>

				<div class="coluna coluna-banners">
					<div id="banners">
						@if($banners)
							@foreach($banners as $banner)
								<div class="banner">
									<img src="assets/images/banners/{{$banner->imagem}}" alt="Pousada Pedra Selada">
								</div>
							@endforeach
						@endif
					</div>
					<a href="#" title="Imagem Anterior" id="banners-nav-prev">Imagem Anterior</a>
					<a href="#" title="Próxima Imagem" id="banners-nav-next">Próxima Imagem</a>					
				</div>
			</div>
		</div>

		<footer>
			<nav>
				<ul>
					<li><a href="suites" class="mn-suites @if(str_is('suites*', Route::currentRouteName())) @endif" title="Suítes"><span>SUÍTES</span></a></li>
					<li><a href="lazer" class="mn-lazer @if(str_is('lazer*', Route::currentRouteName())) @endif" title="Lazer"><span>LAZER</span></a></li>
					<li><a href="culinaria" class="mn-culinaria @if(str_is('culinaria*', Route::currentRouteName())) @endif" title="Culinária"><span>CULINÁRIA</span></a></li>
					<li><a href="promocoes" class="mn-promocoes @if(str_is('promocoes*', Route::currentRouteName())) @endif" title="Promoções"><span>PROMOÇÕES</span></a></li>
					<li><a href="mapa" class="mn-mapa @if(str_is('mapa*', Route::currentRouteName())) @endif" title="Mapa"><span>MAPA</span></a></li>
					<li><a href="traslados" class="mn-traslados @if(str_is('traslados*', Route::currentRouteName())) @endif" title="Traslados"><span>TRASLADOS</span></a></li>
					<li><a href="reservas" class="mn-reservas @if(str_is('reservas*', Route::currentRouteName())) @endif" title="Reservas"><span>RESERVAS</span></a></li>
				</ul>
			</nav>
		</footer>
	</div>

@stop