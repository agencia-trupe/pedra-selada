@section('conteudo')

	<div id="link-mapa">
		<a href="assets/images/layout/mapa-maior.png" title="Como chegar na Pousada Pedra Selada" class="fancy">
			<div id="ampliar">AMPLIAR</div>
			<img src="assets/images/layout/mapa.png" alt="Como chegar na Pousada Pedra Selada">
		</a>
	</div>

	<div id="google-mapa">
		<iframe width="390" height="330" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=22%C2%B015'18.4%22S+44%C2%B026'23.5%22W&amp;aq=&amp;sll=-22.25324,-44.438864&amp;sspn=0.014497,0.026157&amp;t=h&amp;gl=US&amp;ie=UTF8&amp;ll=-22.253235,-44.43886&amp;spn=0.014497,0.026157&amp;z=14&amp;output=embed"></iframe>
		<small>
			<a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=pt-BR&amp;geocode=&amp;q=22%C2%B015'18.4%22S+44%C2%B026'23.5%22W&amp;aq=&amp;sll=-22.25324,-44.438864&amp;sspn=0.014497,0.026157&amp;t=h&amp;gl=US&amp;ie=UTF8&amp;ll=-22.253235,-44.43886&amp;spn=0.014497,0.026157&amp;z=14" title="exibir mapa ampliado" target="_blank">
				EXIBIR MAPA AMPLIADO <img src="assets/images/layout/link-externo.png" alt="exibir mapa ampliado">
			</a>
		</small>		
	</div>

@stop
