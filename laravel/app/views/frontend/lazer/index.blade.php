@section('conteudo')

	<div class="coluna chamada">
		<p>
			A Pousada Pedra Selada é o lugar onde você aproveita cada momento da forma que sempre sonhou: relaxando, lendo, se aventurando, descansando, se exercitando a até se embelezando.
		</p>
	</div>
	<div class="coluna texto">
		<p>
			Você pode aproveitar os esportes ao ar livre como tennis, basquete, corrida; Relaxar na hidro, na sauna ou na piscina; Apreciar a natureza exuberante num passeio a cavalo, no caiaque, numa escalada ou numa trilha; E pode ainda pescar, jogar, ler e até ficar mais bonita com um novo corte de cabelo. Sérgio, o dono, é um renomado cabelereiro do Rio de Janeiro e você ainda pode aproveitar seus momentos de puro deleite para voltar pra casa ainda mais renovada.
		</p>
	</div>

	<ul id="lista-item">
		@if($itemsLazer)
			@foreach($itemsLazer as $item)
				@if(count($item->imagens))
					<li><a href="#" title="{{$item->titulo}}" data-galeria="{{$item->id}}">{{$item->titulo}}</a></li>
				@endif
			@endforeach
		@endif
	</ul>

	<div id="galeria-viewport">
		@if($itemsLazer)
			@foreach($itemsLazer as $item)
				@if(count($item->imagens))
					<div class="galeria galeria-{{$item->id}}">
						@foreach($item->imagens as $img)
							<a href="assets/images/lazer/{{$img->imagem}}" title="{{$img->legenda}}" rel="galeria-{{$img->lazer_id}}" class="fancy"><img src="assets/images/lazer/thumbs/{{$img->imagem}}" alt="{{$img->legenda}}"><div class="overlay"></div></a>
						@endforeach
					</div>
				@endif
			@endforeach
		@endif
	</div>

@stop
