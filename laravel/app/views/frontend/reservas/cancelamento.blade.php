<div id="politica">
	<h1>
		POLÍTICA DE CANCELAMENTO
	</h1>
	<p>
		<span>1.</span> Ao ser efetuada a confirmação da reserva, com o depósito sinal, o hóspede deverá estar ciente de que o saldo restante deverá ser pago imediatamente NA SUA CHEGADA (entrada);
	</p>
	<p>
		<span>2.</span> Em casos de saídas antecipadas, em relação ao período contratado, as diárias NÃO SERÃO RESSARCIDAS. Entretanto, o contratante adquire um crédito do saldo em diárias para futuras hospedagens, a ser utilizado até o prazo máximo de 12 (doze) meses, sujeito a verificação de disponibilidades de apartamento no período desejado, e a variação do valor das diárias para maior ou para menor;
	</p>
	<p>
		<span>3.</span> Cancelamentos de reserva com direito de devolução do dinheiro poderão ocorrer até 7 dias antes do período contratado mas será abatido o valor de 25% (vinte e cinco por cento) para compensação de despesas operacionais;
	</p>
	<p>
		<span>4.</span> Cancelamento de reservas com prazos inferiores a 7 dias implicará na perda dos valores previamente pagos;

	</p>
	<p>
		<span>5.</span> O não comparecimento do hóspede à pousada no dia de entrada e no horário convencionado e na falta de qualquer comunicação prévia (no show) implicará no cancelamento de sua reserva após 12 horas do horário convencionado sem direito a ressarcimento dos pagamentos efetuados;
	</p>
	<p>
		<span>6.</span> O check in (entrada) inicia-se às 12h00 e o check out (saída) ocorre às 12h00, sendo que a permanência do hóspede na suíte após o horário do check out implicará na cobrança adicional de uma diária;
	</p>
	<p>
		<span>7.</span> O direito de reclamar por quaisquer cláusula prescreverá em 30 dias contados da data da confirmação da reserva.
	</p>
</div>