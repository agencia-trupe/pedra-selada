@section('conteudo')
	
	<div class="informacoes">

		<p class="telefones">
			<span><small>(21)</small>3389 3541</span>
			<span><small>(21)</small>9405 1141</span>
			<span><small>(24)</small>9999 6148</span>
		</p>

		<div class="email">
			<a href="mailto:reservas@pedraselada.com.br" title="Envie um e-mail!">RESERVAS@PEDRASELADA.COM.BR</a>
		</div>

		<div class="promocoes">
			<a href="promocoes" title="Consulte nossas promoções">CONSULTE NOSSAS PROMOÇÕES &raquo;</a>
		</div>

		<div class="cancelamento">
			<a href="cancelamento" class="fancy" rel="canc" title="Conheça nossa política de cancelamento">Conheça nossa política de cancelamento.</a>
		</div>

	</div>

	<div class="contato">

		<h2>Envie-nos uma mensagem e entraremos em contato:</h2>

		<form action="{{URL::route('reservas.enviar')}}" method="post">

			<div class="coluna">

				<select name="assunto" required>
					<option value="">ASSUNTO</option>
					<option value="Reservas">Reservas</option>
					<option value="Informações">Informações</option>
					<option value="Dúvidas">Dúvidas</option>
					<option value="Sugestões">Sugestões</option>
				</select>

				<input type="text" name="nome" placeholder="NOME" required>

				<input type="email" name="email" placeholder="E-MAIL" required>

				<input type="text" name="telefone" placeholder="TELEFONE">

			</div>

			<div class="coluna">				
				<textarea name="mensagem" placeholder="MENSAGEM" required></textarea>
			</div>

			<div class="submit">
				<input type="submit" value="ENVIAR">
			</div>

		</form>
	</div>

@stop
