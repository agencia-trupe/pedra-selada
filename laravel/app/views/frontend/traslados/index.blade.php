@section('conteudo')
	
	<div class="informacoes">
		<p>
			A Pousada Pedra Selada possui convênios com empresas de traslados:
		</p>

		<h2>Terrestres:</h2>
		<p class="terrestre">Vans, ônibus e carros.</p>

		<h2>Aéreos:</h2>
		<p class="aereos">Helicóptero.</p>

		<p class="reserva">
			Para reservar seu traslado entre em contato com o departamento de reservas da pousada, fale com <span>Kika</span>:
		</p>

		<p class="telefones">
			<span><small>(21)</small>3389 3541</span>
			<span><small>(21)</small>9405 1141</span>
			<span><small>(24)</small>9999 6148</span>
		</p>
	</div>

	<div class="ilustracao">
		<img src="assets/images/layout/traslados_foto.jpg" alt="Traslados Pousada Pedra Selada">
	</div>

@stop
