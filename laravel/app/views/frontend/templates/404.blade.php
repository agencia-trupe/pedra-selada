<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <meta name="keywords" content="" />

    <title>Pousada Pedra Selada</title>
    <meta name="description" content="">        
    <meta property="og:title" content="Pousada Pedra Selada"/>
    <meta property="og:description" content=""/>

    <meta property="og:site_name" content="Pousada Pedra Selada"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ Request::url() }}"/>

    <base href="{{ url() }}/">
    <script>var BASE = "{{ url() }}"</script>
    
    <?=Assets::CSS(array(
        'vendor/reset-css/reset',
        'css/fontface/stylesheet',
        'vendor/fancybox/source/jquery.fancybox',
        'vendor/pure/grids',
        'css/base'
    ))?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>

    @if(App::environment()=='local')
        <?=Assets::JS(array('js/vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
    @else
        <?=Assets::JS(array('js/vendor/modernizr/modernizr'))?>
    @endif

</head>
<body>
    
    <header>
        <div class="centro">
            <nav>
                <ul>
                    <li><a href="suites" class="mn-suites @if(str_is('suites*', Route::currentRouteName())) ativo @endif" title="Suítes"><span>SUÍTES</span></a></li>
                    <li><a href="lazer" class="mn-lazer @if(str_is('lazer*', Route::currentRouteName())) ativo @endif" title="Lazer"><span>LAZER</span></a></li>
                    <li><a href="culinaria" class="mn-culinaria @if(str_is('culinaria*', Route::currentRouteName())) ativo @endif" title="Culinária"><span>CULINÁRIA</span></a></li>
                    <li><a href="promocoes" class="mn-promocoes @if(str_is('promocoes*', Route::currentRouteName())) ativo @endif" title="Promoções"><span>PROMOÇÕES</span></a></li>
                    <li><a href="mapa" class="mn-mapa @if(str_is('mapa*', Route::currentRouteName())) ativo @endif" title="Mapa"><span>MAPA</span></a></li>
                    <li><a href="traslados" class="mn-traslados @if(str_is('traslados*', Route::currentRouteName())) ativo @endif" title="Traslados"><span>TRASLADOS</span></a></li>
                    <li><a href="reservas" class="mn-reservas @if(str_is('reservas*', Route::currentRouteName())) ativo @endif" title="Reservas"><span>RESERVAS</span></a></li>
                </ul>
            </nav>            
        </div>
    </header>
    <div class="main main-naoencontrado">
        <div class="centro">
            
            <h2>
                Página não encontrada!
            </h2>

        </div>
    </div>
    <footer>
        <nav>
            <ul>
                <li><a href="suites" class="mn-suites @if(str_is('suites*', Route::currentRouteName())) @endif" title="Suítes"><span>SUÍTES</span></a></li>
                <li><a href="lazer" class="mn-lazer @if(str_is('lazer*', Route::currentRouteName())) @endif" title="Lazer"><span>LAZER</span></a></li>
                <li><a href="culinaria" class="mn-culinaria @if(str_is('culinaria*', Route::currentRouteName())) @endif" title="Culinária"><span>CULINÁRIA</span></a></li>
                <li><a href="promocoes" class="mn-promocoes @if(str_is('promocoes*', Route::currentRouteName())) @endif" title="Promoções"><span>PROMOÇÕES</span></a></li>
                <li><a href="mapa" class="mn-mapa @if(str_is('mapa*', Route::currentRouteName())) @endif" title="Mapa"><span>MAPA</span></a></li>
                <li><a href="traslados" class="mn-traslados @if(str_is('traslados*', Route::currentRouteName())) @endif" title="Traslados"><span>TRASLADOS</span></a></li>
                <li><a href="reservas" class="mn-reservas @if(str_is('reservas*', Route::currentRouteName())) @endif" title="Reservas"><span>RESERVAS</span></a></li>
            </ul>
        </nav>
        <div class="assinatura">
            &copy;<?=Date('Y')?> Pedra Selada &bull; Todos os direitos reservados &bull; <a href="http://www.trupe.net" targe="_blank" title="Criação de sites: Trupe Agência Criativa">Criação de sites:</a> <a href="http://www.trupe.net" target="_blank" title="Criação de sites: Trupe Agência Criativa">Trupe Agência Criativa <img src="assets/images/layout/footer_trupe.png" alt="Trupe Agência Criativa"></a>
        </div>
    </footer>

    <?=Assets::JS(array(
        'js/main'
    ))?>

</body>
</html>