@section('conteudo')
	
	<div id="mosaico">
		
		<div id="quadrante-1">
			<a href="assets/images/culinaria/culinaria1.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box1 retanguloH"><img src="assets/images/culinaria/thumbs/culinaria1.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<div class="box2 retanguloH texto"><p>Do café da manhã - delicioso - à ceia ao pé da lareira, um festival de exuberantes sabores à sua espera.</p></div>
			<a href="assets/images/culinaria/culinaria4.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box3 retanguloV"><img src="assets/images/culinaria/thumbs/culinaria4.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria5.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box4 quadradoG"><img src="assets/images/culinaria/thumbs/culinaria5.jpg" alt="Legenda da Imagem"><div class="overlay"></div><div class="legenda bl">Sabores</div></a>
			<a href="assets/images/culinaria/culinaria6.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box5 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria6.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria8.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box6 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria8.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
		</div>

		<div id="quadrante-2">
			<a href="assets/images/culinaria/culinaria2.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box1 quadradoG"><img src="assets/images/culinaria/thumbs/culinaria2.jpg" alt="Legenda da Imagem"><div class="overlay"></div><div class="legenda tl">Temperos</div></a>
			<a href="assets/images/culinaria/culinaria3.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box2 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria3.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria7.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box3 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria7.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria9.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box4 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria9.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<div class="box5 retanguloH texto"><p>Ingredientes fresquíssimos compõem pratos saborosos no fogão a lenha das simpáticas cozinheiras.</p></div>
		</div>

		<div id="quadrante-3">
			<div class="box1 retanguloH texto xtraTop"><p>Um convite irrecusável aos prazeres da boa mesa.</p></div>
			<a href="assets/images/culinaria/culinaria10.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box2 retanguloH"><img src="assets/images/culinaria/thumbs/culinaria10.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria13.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box3 quadradoG"><img src="assets/images/culinaria/thumbs/culinaria13.jpg" alt="Legenda da Imagem"><div class="overlay"></div><div class="legenda br">Aromas</div></a>
			<div class="box4 quadradoG texto xtraLeft"><p>A Pousada Pedra Selada promove festivais gastronômicos sazonais como o Festival do Pinhão, com receita premiada do proprietário Sérgio. Faça sua reserva, confira o próximo festival gastronômico e delicie-se com os sabores exclusivos da Pousada Pedra Selada.</p></div>
			<a href="assets/images/culinaria/culinaria17.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box5 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria17.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria18.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box6 retanguloH"><img src="assets/images/culinaria/thumbs/culinaria18.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria19.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box7 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria19.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
		</div>

		<div id="quadrante-4">
			<a href="assets/images/culinaria/culinaria11.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box1 quadradoG"><img src="assets/images/culinaria/thumbs/culinaria11.jpg" alt="Legenda da Imagem"><div class="overlay"></div><div class="legenda tr">Cores</div></a>
			<a href="assets/images/culinaria/culinaria12.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box2 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria12.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria14.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box3 retanguloV"><img src="assets/images/culinaria/thumbs/culinaria14.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria15.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box4 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria15.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria20.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box5 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria20.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria16.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box6 retanguloV"><img src="assets/images/culinaria/thumbs/culinaria16.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
			<a href="assets/images/culinaria/culinaria21.jpg" title="Legenda da Imagem" rel="culinaria" class="fancy box7 quadradoP"><img src="assets/images/culinaria/thumbs/culinaria21.jpg" alt="Legenda da Imagem"><div class="overlay"></div></a>
		</div>

	</div>

@stop
