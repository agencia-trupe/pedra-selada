@section('conteudo')
	
	<div id="lista-suites" class="pure-g">
		@if(sizeof($suites))
			<?$gridAberto = false?>
			@foreach($suites as $ind => $suite)
				
				@if($ind == 0)
					<a href="suites/detalhes/{{$suite->slug}}" title="{{$suite->titulo}}" class="destaque pure-u-1-2">
						<img src="assets/images/suites/thumbs/grandes/{{$suite->imagem}}" alt="{{$suite->titulo}}">
						<div class="titulo-suite">
							{{mb_strtoupper($suite->titulo)}}
						</div>
					</a>
				@elseif($ind == 1)
					<div class="pure-u-1-2">
						<div class="pure-g">
							<?$gridAberto = true?>
							<a href="suites/detalhes/{{$suite->slug}}" title="{{$suite->titulo}}" class="pure-u-1-2">
								<img src="assets/images/suites/thumbs/pequenas/{{$suite->imagem}}" alt="{{$suite->titulo}}">
								<div class="titulo-suite">
									{{mb_strtoupper($suite->titulo)}}
								</div>
							</a>
				@elseif($ind > 1 && $ind < 4)
							<a href="suites/detalhes/{{$suite->slug}}" title="{{$suite->titulo}}" class="pure-u-1-2">
								<img src="assets/images/suites/thumbs/pequenas/{{$suite->imagem}}" alt="{{$suite->titulo}}">
								<div class="titulo-suite">
									{{mb_strtoupper($suite->titulo)}}
								</div>
							</a>
				@elseif($ind == 4)
							<a href="suites/detalhes/{{$suite->slug}}" title="{{$suite->titulo}}" class="pure-u-1-2">
								<img src="assets/images/suites/thumbs/pequenas/{{$suite->imagem}}" alt="{{$suite->titulo}}">
								<div class="titulo-suite">
									{{mb_strtoupper($suite->titulo)}}
								</div>
							</a>
						</div>
					</div> <!-- Fim do grid pequeno -->
					<?$gridAberto = false?>
				@else
					<a href="suites/detalhes/{{$suite->slug}}" title="{{$suite->titulo}}" class="pure-u-1-4">
						<img src="assets/images/suites/thumbs/pequenas/{{$suite->imagem}}" alt="{{$suite->titulo}}">
						<div class="titulo-suite">
							{{mb_strtoupper($suite->titulo)}}
						</div>
					</a>
				@endif

			@endforeach

			@if($gridAberto)
					</div>
				</div> <!-- Fim do grid pequeno -->
			@endif

		@else

			<h1 class="naoencontrado">Nenhuma Suíte encontrada.</h1>

		@endif
	</div>
@stop
