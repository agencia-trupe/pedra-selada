@section('conteudo')

	<div class="wrapper">

		<section>
			<div class="pad">
				<h2>{{$detalhe->titulo}}</h2>

				<div id="cabecalho">
					<img src="assets/images/suites/cabecalhos/{{$detalhe->imagem_cabecalho}}">
				</div>

				<div id="texto">
					
					<div id="galeria">
						@if($detalhe->imagens)
							@foreach($detalhe->imagens as $imagem)
								<a href="assets/images/suites/imagens/{{$imagem->imagem}}" title="{{$imagem->legenda}}" class="fancy" rel="galeria">
									<img src="assets/images/suites/imagens/thumbs/{{$imagem->imagem}}" alt="{{$imagem->legenda}}">
									<div class="overlay"></div>
								</a>
							@endforeach
						@endif
					</div>

					{{$detalhe->texto}}
					
					<div id="fix"></div>
				</div>
			</div>
		</section>

		<aside>
			@if($suites)
				<ul>
				@foreach($suites as $suite)
					<li><a href="suites/detalhes/{{$suite->slug}}" title="{{$suite->titulo}}" @if($suite->id==$detalhe->id) class="ativo" @endif>{{mb_strtoupper($suite->titulo)}}</a></li>
				@endforeach
				</ul>
			@endif
		</aside>

	</div>

@stop