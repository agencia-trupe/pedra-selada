<?php

use \Image, \Input;

/**
*	Classe wrapper para criação de Thumbs
*	@author Bruno Monteiro
*	@since 10/04/2014
*/
class Thumb
{
	/* Vou receber o nome do input
	*  pegar o input via post
	*  checar dimensões
	*  redimensionar de acordo
	*  retornar o nome da imagem ou false
	*/

	public static function make($input = false, $largura = 0, $altura = 0, $destino = '', $customTimestamp = false)
	{
		if(!$input || !$destino) return false;

		if(Input::hasFile($input)){

			$imagem = Input::file($input);
			$filename = ($customTimestamp !== false) ? $customTimestamp.$imagem->getClientOriginalName() : date('YmdHis').$imagem->getClientOriginalName();
			
			ini_set('memory_limit','256M');
			$imgobj = Image::make(Input::file($input)->getRealPath());

  			if($altura == null || $largura == null){
  				$imgobj->resize($largura, $altura, true, false)->save('assets/images/'.$destino.$filename, 100);
  			}else{

				$ratio = $imgobj->width / $imgobj->height;
	  			$nratio = $largura / $altura;

				if ($ratio > $nratio) {
					// crop height
					$imgobj->resize(null, $altura, true, false)->resizeCanvas($largura, $altura, 'center', false, '#FFFFFF')->save('assets/images/'.$destino.$filename, 100);
				} else {
					// crop width
					$imgobj->resize($largura, null, true, false)->resizeCanvas($largura, $altura, 'center', false, '#FFFFFF')->save('assets/images/'.$destino.$filename, 100);
				}
			}
			$imgobj->destroy();
			return $filename;
		}else{
			die('no file');
			return false;
		}
	}
}