<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLazerImagensTable2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('lazer_imagens', function(Blueprint $table)
		{
			$table->integer('lazer_id')->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('lazer_imagens', function(Blueprint $table)
		{
			$table->dropColumn('lazer_id');
		});
	}

}