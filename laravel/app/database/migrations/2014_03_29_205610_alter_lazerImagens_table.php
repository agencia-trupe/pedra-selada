<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLazerImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('lazer_imagens', function(Blueprint $table)
		{
			$table->string('legenda')->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('lazer_imagens', function(Blueprint $table)
		{
			$table->dropColumn('legenda');
		});
	}

}