<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSuitesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('suites', function(Blueprint $table)
		{
			$table->string('imagem_cabecalho')->after('imagem');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('suites', function(Blueprint $table)
		{
			$table->dropColumn('imagem_cabecalho');
		});
	}

}