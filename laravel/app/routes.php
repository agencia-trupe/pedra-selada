<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('imgfix', array( function(){
	$l = LazerImagem::all();
	$largura = 245;
	$altura = 245;
	foreach ($l as $a => $b) {
		if(file_exists(public_path().'assets/images/lazer/'.$b->imagem)){
			$imgobj = Image::make(public_path().'assets/images/lazer/'.$b->imagem);
			$ratio = $imgobj->width / $imgobj->height;
		  	$nratio = $largura / $altura;

			if ($ratio > $nratio) {
				$imgobj->resize(null, $altura, true, false)->resizeCanvas($largura, $altura, 'center', false, '#FFFFFF')->save(public_path().'assets/images/lazer/thumbs/'.$b->imagem, 100);
			} else {
				$imgobj->resize($largura, null, true, false)->resizeCanvas($largura, $altura, 'center', false, '#FFFFFF')->save(public_path().'assets/images/lazer/thumbs/'.$b->imagem, 100);
			}
			$imgobj->destroy();
		}
	}
	return "OK";
}));

// Rota da página inicial
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('suites', array('as' => 'suites', 'uses' => 'SuitesController@index'));
Route::get('suites/detalhes/{slug?}', array('as' => 'suites.detalhes', 'uses' => 'SuitesController@detalhes'));
Route::get('lazer', array('as' => 'lazer', 'uses' => 'LazerController@index'));
Route::get('promocoes', array('as' => 'promocoes', 'uses' => 'PromocoesController@index'));
Route::get('promocoes/detalhes/{slug?}', array('as' => 'promocoes.detalhes', 'uses' => 'PromocoesController@detalhes'));
Route::get('culinaria', array('as' => 'culinaria', 'uses' => 'CulinariaController@index'));
Route::get('mapa', array('as' => 'mapa', 'uses' => 'MapaController@index'));
Route::get('traslados', array('as' => 'traslados', 'uses' => 'TrasladosController@index'));
Route::get('reservas', array('as' => 'reservas', 'uses' => 'ReservasController@index'));
Route::get('cancelamento', array( function(){
	return View::make('frontend.reservas.cancelamento');
}));
Route::post('enviar', array('as' => 'reservas.enviar', 'uses' => 'ReservasController@enviar'));

// Rota da Home do painel
Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
// Rota da Tela de Login
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

// Rota de Logout
Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
	Route::resource('banners', 'Painel\BannersController');
	Route::resource('suites', 'Painel\SuitesController');
	Route::resource('suitesImagens', 'Painel\SuitesImagensController');
	Route::resource('lazer', 'Painel\LazerController');
	Route::resource('lazerImagens', 'Painel\LazerImagensController');
	Route::resource('promocoes', 'Painel\PromocoesController');
    Route::resource('usuarios', 'Painel\UsuariosController');
});