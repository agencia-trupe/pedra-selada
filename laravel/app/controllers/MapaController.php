<?php

class MapaController extends BaseController {

	public function index()
	{
		$this->layout->with('titulo', 'Mapa');
		$this->layout->content = View::make('frontend.mapa.index');
	}

}
