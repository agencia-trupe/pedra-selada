<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, File, Image, Lazer;

class LazerController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.lazer.index')->with('registros', Lazer::orderBy('ordem', 'ASC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.lazer.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Lazer;

		$object->titulo = Input::get('titulo');

		if(!$object->titulo){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Item de Lazer!'));
		}else{
			try {

				$object->save();

				$object->slug = Str::slug($object->id.'_'.$object->titulo);
				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Item de Lazer criado com sucesso.');
				return Redirect::route('painel.lazer.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Item de Lazer!'));	

			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.lazer.edit')->with('registro', Lazer::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Lazer::find($id);

		$object->titulo = Input::get('titulo');
		
		if(!$object->titulo){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Item de Lazer!'));
		}else{
			try {

				$object->save();

				$object->slug = Str::slug($object->id.'_'.$object->titulo);
				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Item de Lazer alterado com sucesso.');
				return Redirect::route('painel.lazer.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Item de Lazer!'));	

			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Lazer::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Item de Lazer removido com sucesso.');

		return Redirect::route('painel.lazer.index');
	}

}