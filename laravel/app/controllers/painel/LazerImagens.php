<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, File, Image, Lazer, LazerImagem;

class LazerImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteRegistros = 4;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$lazer_id = Input::get('lazer_id');

		if(!$lazer_id)
			return Redirect::back();

		$this->layout->content = View::make('backend.lazerImagens.index')->with('registros', LazerImagem::orderBy('ordem', 'ASC')->where('lazer_id', $lazer_id)->get())
																  ->with('lazer', Lazer::find($lazer_id))
																  ->with('limiteRegistros', $this->limiteRegistros);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$lazer_id = Input::get('lazer_id');

		if(!$lazer_id)
			return Redirect::back();

		$this->layout->content = View::make('backend.lazerImagens.form')->with('lazer', Lazer::find($lazer_id));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new LazerImagem;

		$object->legenda = Input::get('legenda');
		$object->lazer_id = Input::get('lazer_id');

		if(sizeof(LazerImagem::where('lazer_id', '=', $object->lazer_id)->get()) >= $this->limiteRegistros){
			Session::flash('formulario', array('legenda' => $object->legenda, 'lazer_id' => $object->lazer_id));
			return Redirect::back()->withErrors(array('Número máximo de imagens já atingido!'));
		}

		Thumb::make('imagem', 900, null, 'lazer/');
		$imagem = Thumb::make('imagem', 245, 245, 'lazer/thumbs/');
		if($imagem) $object->imagem = $imagem;
		
		if(!$object->imagem){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem de Lazer!'));
		}else{
			try {

				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Imagem de Lazer criada com sucesso.');
				return Redirect::route('painel.lazerImagens.index', array('lazer_id' => $object->lazer_id));

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Imagem de Lazer!'));	

			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$registro = LazerImagem::find($id);
		$lazer = Lazer::find($registro->lazer_id);
		$this->layout->content = View::make('backend.lazerImagens.edit')->with('registro', $registro)->with('lazer', $lazer);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = LazerImagem::find($id);

		$object->legenda = Input::get('legenda');
		$object->lazer_id = Input::get('lazer_id');

		Thumb::make('imagem', 900, null, 'lazer/');
		$imagem = Thumb::make('imagem', 245, 245, 'lazer/thumbs/');
		if($imagem) $object->imagem = $imagem;
		
		if(!$object->imagem){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem de Lazer!'));
		}else{
			try {

				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Imagem de Lazer alterada com sucesso.');
				return Redirect::route('painel.lazerImagens.index', array('lazer_id' => $object->lazer_id));

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Imagem de Lazer!'));	

			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$lazer_id = Input::get('lazer_id');

		$object = LazerImagem::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem de Lazer removida com sucesso.');

		return Redirect::route('painel.lazerImagens.index', array('lazer_id' => $lazer_id));
	}

}