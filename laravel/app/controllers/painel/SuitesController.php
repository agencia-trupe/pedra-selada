<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, File, Image, Thumb, Suite;

class SuitesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.suites.index')->with('registros', Suite::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.suites.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Suite;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$ts = date('YmdHis');
		$thumb1 = Thumb::make('imagem', 490, 360, 'suites/thumbs/grandes/', $ts);
		$thumb2 = Thumb::make('imagem', 245, 180, 'suites/thumbs/pequenas/', $ts);
		if($thumb1) $object->imagem = $thumb1;

		$cabecalho = Thumb::make('cabecalho', 800, 200, 'suites/cabecalhos/');
		if($cabecalho) $object->imagem_cabecalho = $cabecalho;
	
		if(!$object->titulo || !$object->imagem){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Suíte!'));
		}else{
			try {

				$object->save();

				$object->slug = Str::slug($object->id.'_'.$object->titulo);
				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Suíte criada com sucesso.');
				return Redirect::route('painel.suites.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Suíte!'));	

			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.suites.edit')->with('registro', Suite::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Suite::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$ts = date('YmdHis');
		$thumb1 = Thumb::make('imagem', 490, 360, 'suites/thumbs/grandes/', $ts);
		$thumb2 = Thumb::make('imagem', 245, 180, 'suites/thumbs/pequenas/', $ts);
		if($thumb1) $object->imagem = $thumb1;

		$cabecalho = Thumb::make('cabecalho', 800, 200, 'suites/cabecalhos/');
		if($cabecalho) $object->imagem_cabecalho = $cabecalho;
		
		if(!$object->titulo){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Suíte!'));
		}else{
			try {

				$object->save();

				$object->slug = Str::slug($object->id.'_'.$object->titulo);
				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Suíte alterada com sucesso.');
				return Redirect::route('painel.suites.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Suíte!'));	

			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Suite::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Suíte removida com sucesso.');

		return Redirect::route('painel.suites.index');
	}

}