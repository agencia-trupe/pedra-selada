<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, File, Image, Promocao;

class PromocoesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.promocoes.index')->with('registros', Promocao::orderBy('ordem', 'ASC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.promocoes.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Promocao;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$imagem = Thumb::make('imagem', 980, 160, 'promocoes/');
		if($imagem) $object->imagem = $imagem;
		
		if(!$object->imagem){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Promoção!'));
		}else{
			try {

				$object->save();

				$object->slug = Str::slug($object->id.'-'.$object->titulo);
				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Promoção criada com sucesso.');
				return Redirect::route('painel.promocoes.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Promoção!'));	

			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.promocoes.edit')->with('registro', Promocao::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Promocao::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$imagem = Thumb::make('imagem', 980, 160, 'promocoes/');
		if($imagem) $object->imagem = $imagem;
		
		if(!$object->imagem){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Promoção!'));
		}else{
			try {

				$object->save();

				$object->slug = Str::slug($object->id.'-'.$object->titulo);
				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Promoção alterada com sucesso.');
				return Redirect::route('painel.promocoes.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Promoção!'));	

			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Promocao::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Promoção removida com sucesso.');

		return Redirect::route('painel.promocoes.index');
	}

}