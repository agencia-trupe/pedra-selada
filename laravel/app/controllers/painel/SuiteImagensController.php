<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, File, Image, Thumb, Suite, SuiteImagem;

class SuitesImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$id_suite = Input::get('id_suite');

		if(!$id_suite)
			return Redirect::back();

		$this->layout->content = View::make('backend.suitesImagens.index')->with('registros', SuiteImagem::orderBy('ordem', 'asc')->where('suites_id', $id_suite)->get())
																		  ->with('suite', Suite::find($id_suite));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$id_suite = Input::get('id_suite');

		if(!$id_suite)
			return Redirect::back();

		$this->layout->content = View::make('backend.suitesImagens.form')->with('suite', Suite::find($id_suite));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$id_suite = Input::get('id_suite');

		if(!$id_suite)
			return Redirect::back();

		$object = new SuiteImagem;

		$object->legenda = Input::get('legenda');
		$object->suites_id = $id_suite;
		
		$t = date('dmyHis');
		$imagem = Thumb::make('imagem', 175, 130, 'suites/imagens/thumbs/', $t);
		Thumb::make('imagem', 880, null, 'suites/imagens/', $t);
		if($imagem) $object->imagem = $imagem;

		if(!$object->imagem){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));
		}else{
			try {

				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Imagem criada com sucesso.');
				return Redirect::route('painel.suitesImagens.index', array('id_suite' => $id_suite));

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$suiteImagem = SuiteImagem::find($id);
		
		$this->layout->content = View::make('backend.suitesImagens.edit')->with('registro', $suiteImagem)
																  		 ->with('suite', $suiteImagem->suite()->first());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$id_suite = Input::get('id_suite');

		$object = SuiteImagem::find($id);

		$object->legenda = Input::get('legenda');
		$object->suites_id = $id_suite;

		$t = date('dmyHis');
		Thumb::make('imagem', 880, null, 'suites/imagens/', $t);
		$imagem = Thumb::make('imagem', 175, 130, 'suites/imagens/thumbs/', $t);
		if($imagem) $object->imagem = $imagem;

		if(!$object->suites_id){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));
		}else{
			try {

				$object->save();

				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Imagem alterada com sucesso.');
				return Redirect::route('painel.suitesImagens.index', array('id_suite' => $object->suites_id));

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = SuiteImagem::find($id);
		$id_suite = $object->suites_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removida com sucesso.');

		return Redirect::route('painel.suitesImagens.index', array('id_suite' => $id_suite));
	}

}