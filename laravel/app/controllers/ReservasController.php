<?php

class ReservasController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('titulo', 'Informações e Reservas');
		$this->layout->content = View::make('frontend.reservas.index');
	}

	public function enviar()
	{
		$data['assunto'] = Request::get('assunto');
		$data['nome'] = Request::get('nome');
		$data['email'] = Request::get('email');
		$data['telefone'] = Request::get('telefone');
		$data['mensagem'] = Request::get('mensagem');

		if($data['assunto'] && $data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('reservas@pedraselada.com.br', 'Pousada Pedra Selada')
			    		->subject('Contato via site - '.$data['assunto'])
			    		->replyTo($data['email'], $data['nome']);
			});
		}
		
		Session::flash('envio', true);
		
		return Redirect::to('reservas');
	}	
}
