<?php

use \Promocao;

class PromocoesController extends BaseController {

	public function index()
	{
		$this->layout->with('titulo', 'Promoções');
		$this->layout->content = View::make('frontend.promocoes.index')->with('promocoes', Promocao::orderBy('ordem', 'ASC')->get());
	}

	public function detalhes($slug = false)
	{
		if(!$slug)
			App::abort('404');

		$promo = Promocao::where('slug', '=', $slug)->first();
		
		if(!$promo)
			App::abort('404');

		$this->layout->with('titulo', 'Promoções')->with('css', 'css/promocoes');
		$this->layout->content = View::make('frontend.promocoes.detalhes')->with('detalhe', $promo);
	}
}
