<?php

use \Suite;

class SuitesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('titulo', 'Suítes');
		$this->layout->content = View::make('frontend.suites.index')->with('suites', Suite::orderBy('ordem', 'asc')->get());
	}

	public function detalhes($slug = false)
	{
		$this->layout->with('titulo', 'Suítes')->with('css', 'css/suites');

		$detalhe = Suite::where('slug', '=', $slug)->first();

		if(!$slug || is_null($detalhe))
			App::abort('404');

		$this->layout->content = View::make('frontend.suites.detalhes')->with('suites', Suite::orderBy('ordem', 'asc')->get())
																	   ->with('detalhe', $detalhe);
	}
}
