<?php

use \Banner;

class HomeController extends BaseController {

	public function index()
	{
		$this->layout->content = View::make('frontend.home.index')->with('banners', Banner::orderBy('ordem', 'ASC')->get());
	}

}