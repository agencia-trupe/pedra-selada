<?php

class CulinariaController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('titulo', 'Culinária');
		$this->layout->content = View::make('frontend.culinaria.index');
	}

}
