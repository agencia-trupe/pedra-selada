<?php

use \Lazer;

class LazerController extends BaseController {

	public function index()
	{
		$this->layout->with('titulo', 'Lazer');
		$this->layout->content = View::make('frontend.lazer.index')->with('itemsLazer', Lazer::orderBy('ordem', 'asc')->get());
	}

}
