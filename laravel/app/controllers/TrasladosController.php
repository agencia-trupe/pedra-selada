<?php

class TrasladosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('titulo', 'Traslados');
		$this->layout->content = View::make('frontend.traslados.index');
	}

}
